package com.imergex.firebasedemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseApp.initializeApp(this)
        mAuth = FirebaseAuth.getInstance()

        loginTapped()
    }

    fun loginTapped() {
        btn_login.setOnClickListener {

            val email = txt_email.text.toString()
            val password = txt_passowrd.text.toString()

            mAuth?.signInWithEmailAndPassword(email, password)?.addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    //Logged in
                    Toast.makeText(this, "Logged In", Toast.LENGTH_LONG).show()
                } else {
                    //Invalid
                    Toast.makeText(this, "Invalid", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
    }
}
