package com.imergex.enrollmentregtration

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        popUp()
    }

    fun popUp() {
        btn_register.setOnClickListener {
            val firstName = txt_firstName.text
            Toast.makeText(this, "Welcome $firstName to BASTATEU", Toast.LENGTH_LONG).show()
        }
    }
}
